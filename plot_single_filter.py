import utils.sonnet_simulation_tools
import utils.filter_analysis_tools
import skrf as rf
import os

directory = r"data"
filename = "discrete_cap_mmwaveresonator_invustrip_r100_2bends_with_SiN_Membrane_param1.s3p"
plot_db = True
plot_power = False

file_path = os.path.join(directory, filename)

# Read current touchstone file
touchstone_file = rf.Touchstone(file_path)
# Get touchstone variables from comments
variables = touchstone_file.get_comment_variables()
variables_string = ""
n = 1
for variable in variables:
    if variable == list(variables.keys())[-1]:
        variables_string += variable + "=" + str(variables[variable][0])
        continue
    variables_string += variable + "=" + str(variables[variable][0]) + ", "
    if len(variables_string) > 60 * n:
        n += 1
        variables_string += "\n"

network = utils.sonnet_simulation_tools.make_renormalised_network(
    data_file_path=file_path, n_ports=3
)

frequency = network.f

utils.sonnet_simulation_tools.plot_network_s_params(
    network=network,
    from_port=1,
    to_port=2,
    plot_title="Filter with " + variables_string,
    plot_db=plot_db,
    plot_power=plot_power,
)
utils.sonnet_simulation_tools.plot_network_s_params(
    network=network,
    from_port=1,
    to_port=3,
    plot_title="Filter with " + variables_string,
    plot_db=plot_db,
    plot_power=plot_power,
)
