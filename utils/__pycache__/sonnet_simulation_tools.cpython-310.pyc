o
    v�Oc4  �                   @   s�   d dl Zd dlmZ d dlZd dlZd dl	m
Z
 defdd�Zdededefd	d
�Zdedejfdd�Zdedejfdd�Zdededejfdd�Zdedefdd�Zdejdededdddddddfdd�Zd%d!ejd"ejfd#d$�ZdS )&�    N)�stats�stringc                 C   s  d}i }|t | �k r�| �d|t | ��}|dkr	 |S | �d||�}| ||� �� }| |d  dkrE| �d|t | ��}| |d |� �� }n9| �d|t | ��}| �d||�}|dkr`| �d	||�}t| |d
 |� �� �}	t| |d |� �d��}
t|	|
�}|||< |}|t | �k s
|S )z�
    Function to extract port variables from comments in touchstone data.

    :param string: String for a given line in a comment in a touchstone file.
    :return:
    r   �=������ �   �(z) �+�-�   �   z ))�len�find�rfind�strip�float�complex)r   Zpos_in_lineZport_dictionaryZ
equals_posZvar_name_start_posZvar_nameZvar_end_pos�varZ	split_posZvar_realZvar_imag� r   �iC:\Python_Projects\Sonnet_simulation_data\SPT_SLIM\Filter\Filter_fitting\utils\sonnet_simulation_tools.py�get_port_parameters   s2   ��
�"r   �	file_path�port_number�returnc                 C   sX  t | d��}d}|D ]}d| |v r|d7 }q
W d  � n1 s!w   Y  t | d��s}tj|d�tj|td�d�}d}|D ] }d| |v r^t|�}|d	 |d	 |< |d
 |d
 |< |d7 }q>t�|d
 j�}tt�|d
 j�t�	|d
 j� �d }	t�|d
 j
�}
tt�|d
 j
�t�	|d
 j
� �d }W d  � n1 s�w   Y  ||
|	|gS )aO  
    Function to return the mean impedance of a given port across a frequency range from a touchstone file.
    Impedance returned in list

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for.
    :return: List of impedance values in format: [r_mean, x_mean, Rerr, Xerr]
    �rr   �! Pr   N)�shape�r   Zdtype)�F�Z0r   r   r   )�open�np�emptyr   r   Zmean�real�abs�max�min�imag)r   r   �readerZimpedance_data_points�line�port_impedances�
data_point�current_port_paramsZr_meanZr_errZx_meanZx_errr   r   r   �get_mean_port_impedance7   s@   
���
��"��"���r-   c                 C   ��   t | d��}d}|D ]}d| |v r|d7 }q
W d  � n1 s!w   Y  t | d��+}tj|td�}d}|D ]}d| |v rMt|�}|d ||< |d7 }q7W d  � |S 1 sYw   Y  |S )aM  
    Function to return an array of port Z0 values extracted from an output
    touchstone file of a sonnet simulation. These Z0 values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    r   r   r   r   Nr   r   �r    r!   r"   r   r   )r   r   r(   �data_pointsr)   r*   r+   r,   r   r   r   �get_port_z0_arrayf   �,   	�����
��r1   c                 C   r.   )a  
    Function to return an array of frequency values at which the port z0 and
    Eeff were evaluated in the sonnet touchstone file.

    :param file_path: Path to data file
    :param port_number: Port number to determine the mean impedance for. String of an integer.
    :return:
    r   r   r   r   Nr   r   )r    r!   r"   r   r   )r   r   r(   r0   r)   Zport_frequenciesr+   r,   r   r   r   �get_port_f_array�   s,   
�����
��r3   c                 C   r.   )aQ  
    Function to return an array of port Eeff values extracted from an output
    touchstone file of a sonnet simulation. These Eeff values correspond to the
    port impedance at a certain frequency. The corresponding frequencies can be
    extracted using the get_port_f_array() method.

    port_number is a string of an integer.
    r   r   r   r   Nr   ZEeffr/   )r   r   r(   r0   r)   Zport_permittivitiesr+   r,   r   r   r   �get_port_Eeff_array�   r2   r4   �data_file_path�n_portsc                 C   s�   t �| �}tj�| d�\}}}}t||�g}|dkr-tj�| d�\}}	}
}|�t||	�� |dkrDtj�| d�\}}}}|�t||�� |dkr[tj�| d�\}}}}|�t||�� |�|� |S )aF  
    Function to create a scikit-rf network object from a touchstone file and renormalise the S parameters by the
    mean port impedance over the frequency sweep range.

    :param data_file_path: Path to touchstone data file.
    :param n_ports: number of ports in data file. One port: 1, two ports: 2 etc.
    :return:
    �1r   �2r   �3r   �4)�rf�Network�utils�sonnet_simulation_toolsr-   r   �appendZrenormalize)r5   r6   �networkZport_1_rZport_1_xZport_1_r_errZport_1_x_errZmean_port_z0_listZport_2_rZport_2_xZport_2_r_errZport_2_x_errZport_3_rZport_3_xZport_3_r_errZport_3_x_errZport_4_rZport_4_xZport_4_r_errZport_4_x_errr   r   r   �make_renormalised_network�   sD   
����
rA   r@   �	from_port�to_port�
plot_title� �plot_dbT�
plot_powerc                 C   s  |d }|d }|r| j dd�||f }d|d |d f }n3|r;t�| jdd�||f �d }d|d |d f }nt�| jdd�||f �}d|d |d f }tjddd� tj| jd	 |d
d� t�d� t�	|� tj
ddd� t�|� t��  t��  dS )a�  
    Function to plot the desired scattering parameters of a given scikit-rf network vs frequency in GHz.

    :param network: Network object with scattering parameter arrays.
    :param from_port: Input port number for scattering parameter. Port 1: 1, Port 2: 2.
    :param to_port: output port number for scattering parameter. Port 1: 1, Port 2: 2.
    :param plot_title: Title to give the plot, default is an empty string.
    :param plot_db: Boolean to plot S param in decibels.
    :param plot_power: Boolean to plot S param in power (magnitude squared). Note if plot_db must be False, otherwise
    the plot will be in dB. If both are false, S param will be plotted in absolute magnitude.
    :return:
    r   NzS%i%i Magnitude (dB)r   zS%i%i PowerzS%i%i Magnitude��   �   )Znum�figsizeg��&�.>r
   )�	linestylezFrequency (GHz)zcenter left)g        g333333�?)ZlocZbbox_to_anchor)Zs_dbr!   Zabsolute�s�plt�figure�plot�f�xlabel�ylabel�legend�titleZtight_layout�show)r@   rB   rC   rD   rF   rG   Zs_param�y_labelr   r   r   �plot_network_s_params�   s&   


rX   �X Data�Y Data�x_data�y_datac              
   C   s�   t �| |�}|rGtjdd� tj| |dddd� tj| |j|j|   dd|j|j|jd	 |j|j	f d
� t�
|� t�|� t��  t��  |S )aY  
    Function to fit a linear regression to x and y data and return the scipy LinregressResult object. See
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html for attributes and methods.

    :param x_data: X data to fit to.
    :param y_data: Y data to fit to.
    :param plot_graph: Boolean to
    :param x_label: Label for x axis of plot.
    :param y_label: Label for y axis of plot.
    :return: result: LinregressResult instance. The return value is an object with the following attributes:
        slope: float
            Slope of the regression line.
        intercept: float
            Intercept of the regression line.
        rvalue: float
            The Pearson correlation coefficient. The square of ``rvalue``
            is equal to the coefficient of determination.
        pvalue: float
            The p-value for a hypothesis test whose null hypothesis is
            that the slope is zero, using Wald Test with t-distribution of
            the test statistic. See `alternative` above for alternative
            hypotheses.
        stderr: float
            Standard error of the estimated slope (gradient), under the
            assumption of residual normality.
        intercept_stderr: float
            Standard error of the estimated intercept, under the assumption
            of residual normality.
    rH   )rK   Znone�oZData)rL   Zmarker�labelr   zMy = mx + c fit with:
m=%.2e
c=%.2e
R$^2$=%.2e
$\sigma_m$=%.2e
$\sigma_c$=%.2er   )Zcolorr^   )r   Z
linregressrN   rO   rP   Z	interceptZslopeZrvalue�stderrZintercept_stderrrR   rS   rT   rV   )r[   r\   Z
plot_graphZx_labelrW   �resultr   r   r   �fit_linear_regression(  s"   ��

ra   )TrY   rZ   )Znumpyr!   Zmatplotlib.pyplotZpyplotrN   �skrfr;   �utils.filter_analysis_toolsr=   Zscipyr   �strr   �listr-   Zndarrayr1   r3   r4   �intrA   r<   rX   ra   r   r   r   r   �<module>   s6    // 6������
�.