lmfit==1.0.3
matplotlib==3.5.2
numpy==1.22.4
pandas==1.4.3
scipy==1.9.0

scikit-rf~=0.23.1
